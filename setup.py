from setuptools import setup

setup(
    name='pyAtlasBoneSegmentation',
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    packages=['pyAtlasBoneSegmentation',
             ],
    scripts=['pyabs.sh']
)
